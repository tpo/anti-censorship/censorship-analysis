Usage: default_bridge_metrics.py --cc=CC [options]

Options:
  -h, --help            show this help message and exit
  --cc=CC, --country=CC
                        country code for metrics
  -p PLOT, --plot=PLOT  filename for plot output
  --start=START         date to start analysis from
  --end=END             date to end analysis
