#!/usr/bin/env python3
#
# Determine the number of connections from users in a country code to our default bridges
#

from optparse import OptionParser
import sys
import os
import json
import tarfile
from urllib.request import urlopen, urlretrieve
import stem.descriptor
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import matplotlib.patches as mpatches
import pandas as pd
from pycountry import countries

# The set of default bridges we ship with Tor Browser
default_bridges = set(["Lisbeth", "frosty", "dragon", "griinchux",
        "zipfelmuetze", "smallerRichard", "KauBridgePale", "KauBridgeBlue",
        "KauBridgeDot", "GeorgetownPontem", "hopperlab", "dktoke",
        "PraxedisGuerrero"])

# URL containing a json representation of available CollecTor data
collector_url = "https://collector.torproject.org"

class Metrics(object):

    def __init__(self, start, end, cc):
        self.counts = {}
        for b in default_bridges:
            self.counts[b] = {}
            self.counts[b]["dates"] = []
            self.counts[b]["count"] = []

        self.start = start
        self.end = end
        self.cc = cc

    def add(self, nickname, date, count):
        self.counts[nickname]
        if date < self.start or date > self.end:
            return
        if not date.date() in self.counts[nickname]["dates"]:
            self.counts[nickname]["dates"].append(date.date())
            self.counts[nickname]["count"].append(count)

    def plot(self, filename):
        for bridge in self.counts:
            df=pd.DataFrame()
            df['name'] = self.counts[bridge]["dates"]
            df['value'] = self.counts[bridge]["count"]
            df.sort_values('name', ascending = False, inplace= True)
            plt.plot(df['name'], df['value'], label=bridge)

        plt.gcf().autofmt_xdate()
        plt.legend(bbox_to_anchor=(1,0), loc="lower left", title="default bridges")
        plt.xlabel('Date')
        country = countries.get(alpha_2=self.cc).name
        plt.title("Default bridge users from {}".format(country))
        plt.savefig(filename, bbox_inches='tight')

    def process_extrainfo_file(self, filename):
        bridges = stem.descriptor.parse_file(filename)
        for bridge in bridges:
            if bridge.nickname in default_bridges:
                if not bridge.bridge_ips:
                    continue
                count = 0
                if self.cc in bridge.bridge_ips:
                    count = bridge.bridge_ips[self.cc]
                self.add(bridge.nickname, bridge.published, count)

# Downloads and extracts a tarball of archived CollecTor data
def fetch_tarball(link):
    print("retrieving link: {}".format(link), file=sys.stderr)
    filename, _ = urlretrieve(link)
    print("extracting tarball (this could take some time)...", file=sys.stderr)
    archive = tarfile.open(filename)
    archive.extractall()

# Returns a subdirectory json object with the given path value
def get_subdir(dir, path):
    for d in dir['directories']:
        if d['path'] == path:
            return d

# Fetches archived CollecTor bridge extrainfo descriptors for the specified time frame
# and extracts the tarballs into the current working directory
# Returns a list of the extracted directories
def collect_data(start, end):
    page = urlopen(collector_url+"/index/index.json").read().decode('utf-8')
    data = json.loads(page)

    dirs = []
    d = get_subdir(data, "archive")
    d = get_subdir(d, "bridge-descriptors")
    d = get_subdir(d, "extra-infos")
    for file in d['files']:
        last_published = dt.datetime.strptime(file['last_published'], "%Y-%m-%d %H:%M")
        first_published = dt.datetime.strptime(file['first_published'], "%Y-%m-%d %H:%M")
        if last_published >= start and last_published <= end:
            f = file['path']
            fetch_tarball("{}/archive/bridge-descriptors/extra-infos/{}".format(collector_url, f))
            dirs.append(file['path'].split('.')[0])
        elif first_published >= start and first_published <= end:
            f = file['path']
            fetch_tarball("{}/archive/bridge-descriptors/extra-infos/{}".format(collector_url, f))
            dirs.append(file['path'].split('.')[0])

    return dirs


def process_descriptors(path, metrics):
    print("processing extrainfo descriptors (this could take some time)...", file=sys.stderr)
    for root, _, files in os.walk(path):
        for f in files:
            metrics.process_extrainfo_file(os.path.join(root,f))

def main():

    today = dt.datetime.now().strftime("%Y-%m-%d")
    ago = (dt.datetime.now() - dt.timedelta(days=30)).strftime("%Y-%m-%d")

    parser = OptionParser(usage="usage: %prog --cc=CC [options]")
    parser.add_option("--cc", "--country", help="country code for metrics",
            default=None, dest="cc")
    parser.add_option("-p", "--plot", help="filename for plot output",
            default = "default-bridge-users.png", dest="plot")
    parser.add_option("--start", help="date to start analysis from",
            default = ago, dest="start")
    parser.add_option("--end", help="date to end analysis",
            default = today, dest="end")
    (options, args) = parser.parse_args()


    if options.cc is None:
        parser.print_help()
        sys.exit(1)

    start = dt.datetime.strptime(options.start, "%Y-%m-%d")
    end = dt.datetime.strptime(options.end, "%Y-%m-%d")
    metrics = Metrics(start, end, options.cc.lower())
    # Get CollecTor data for time frame
    dirs = collect_data(start, end)
    for d in dirs:
        process_descriptors(d, metrics)

    metrics.plot(options.plot)

if __name__ == '__main__':
    main()

