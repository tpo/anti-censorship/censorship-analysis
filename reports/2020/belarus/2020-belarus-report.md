# Tor blocking events in Belarus from 2020-2021
## Date: 21 April 2021

Following the presidential election in 2020 and amid ongoing protests, Belarus has been ramping up their Internet censorship efforts by using increasingly sophisticated methods to block access to anti-censorship tools.

We first detected blocking of the Tor network in Belarus in August of 2020, occurring alongside [widespread Internet shutdowns](https://ooni.org/post/2020-belarus-internet-outages-website-censorship/). This initial period of blocking was short-lived and connectivity to the Tor network was returned a few days later. Then, in October of 2020, Belarusian censors undertook a renewed effort to block access to Tor. This consisted of blocking access to Tor relays and directory authorities. Starting in February of 2021, we saw increasingly sophisticated enumeration and blocks of Tor's bridges and bridge distribution infrastructure.

In most places, censorship is not consistent across different ISPs or autonomous systems. We have data for the following ASes in Belarus, listed here along with their organization name (according to the latest whois data):

| Organization Name | AS# |
| --- | --- |
| Beltelecom | 6697 |
| Business Network | 12406 |
| Mobile TeleSystems | 25106 |
| COSMOS TV | 31143 |
| A1 | 42772 |
| KT MAZ | 43232 |
| BeST | 44087 |
| Flynet | 49711 |
| Vash Internet | 50294 |
| GARANT | 50334 |
| Qline | 50438 |
| United Networks | 50685 |
| Unitary Services | 59861 |

## Blocking Tor relays

Our usage metrics data of directly connecting users to the Tor network is an early indicator that a region is blocking access to Tor. We typically see a drop in directly connecting users from the region that corresponds with a rise in users that connect to Tor through bridges.

Around August 9th, 2020 the number of directly connecting Tor users dropped significantly, and the number of bridge users rose to more than six times the previous count.

![Directly connecting users in August](userstats-relay-country-by-august.png)

![Bridge users in August](userstats-bridge-by-august.png)

Usage counts returned to their previous normal levels on August 11th and remained steady for several months, until renewed efforts to block Tor started in October of 2020.

![Directly connecting users in October](userstats-relay-country-by-october.png)

![Bridge users in October](userstats-bridge-by-october.png)

Here we saw the number of directly connecting users plummet and again a rise in bridge users, that continues to the time of writing this report.

Blocking the Tor directory authorities and the fallback directories will effectively prevent new users (or users with an expired guard) from downloading the Tor consensus, but so will the complete blocking of all Tor relays.

We noticed an attempt to enumerate and block access to all Tor relays and kept their block list relatively up to date with the current consensus. A volunteer ran reachability scans of the entire Tor consensus from within the A1 ISP's space on two different days, February 25th and March 3rd, to measure how agile the censor was at updating their Tor relays block list. Of the 6647 relays in the public Tor consensus on February 25th, 6647 of connection attempts to these relays from the A1 ISP in Belarus timed out with an error consistent with the blocking pattern described below. On March 3rd, 6966 out of 7064 relays in the consensus timed out.

As expected, there was some churn as new relays joined the network and old relays dropped off. There were a total of 952 relays that were present in the March 3rd consensus but not present in the February 25th consensus. These relays may have been new, or they may have been existing relays that were just not online at the time of our first measurement on February 25th. Of these emergent relays, all but one timed out when a direction connection attempt was made.
The reachability data of Tor relays suggests to us that the censor is automatically updating its block list based on the Tor consensus. Almost all of the relays that were not in the earlier consensus were blocked.

We are omitting the packet captures from our volunteer for privacy reasons, but they did provide us some insight on the blocking mechanism used. Failed connections to blocked bridges and relays shared the following pattern:

```
Tor user                  relay
        ------- SYN ----->
        <---- SYN ACK ----
        ------- ACK ----->
        -- Client Hello ->
        -- Client Hello -> (retransmission)
        <---- SYN ACK ---- (retransmission)
        -- Client Hello -> (retransmission)
        <---- SYN ACK ---- (retransmission)
        ------- FIN ----->
```
where it appears that the final ACK in the TCP handshake is being dropped, causing the client to eventually give up on the connection. This resulted in an SSL error in our logs which at first led us to believe the blocking was occurring at the TLS layer, but closer inspection shows us that the TCP handshake is never completed due to the missing final ACK packet.

## Blocking Tor websites

According to OONI data from August 2020 to now, https://torproject.org appears to have been reachable until more recently. There were successful connections to the Tor Project website from Beltelecom until late February, and the last successful connection from A1 was in late October. It's hard to pinpoint the precise times during which https://torproject.org was unreachable, but it appears likely that the website is currently blocked in Belarus.

![Tor Project website connection attempts](reachability-www.png)

## Blocking Default Bridges

Tor distributes bridges (non-public relays) to users in censored regions through a variety of different distribution methods. The first line of anti-censorship for many are the default bridges hard-coded and shipped with Tor Browser. Our default bridges are specifically selected for their reliability and capacity to handle large volumes of user traffic. We ship default bridges for two different transports: obfs4 and meek in Tor Browser.

In February, we witnessed a sudden decrease in the number of connections from Belarus to our default obfs4 bridges, and a switch in our transport usage from being majority obfs4 to the built-in meek bridge, that was likely a result of our default obfs4 bridges getting blocked.

![Default bridge users in February](userstats-default-bridge-by-february.png)

![Bridge users in February by transport](userstats-bridge-combined-by-february.png)

Looking at [OONI data](https://explorer.ooni.org/search?until=2021-04-14&since=2021-02-01&probe_cc=BY&test_name=tor) of the reachability of our default bridges by ISP, we see that reachability varies by autonomous system (AS), but the reachability of our default bridges in all major ASes drops in February and March.

There is an initial drop in reachability for most ASes that occurs in the first half of February that corresponds with our usage metrics of default bridges above. This is followed by a drop in the reachability of default bridges from AS6697 in early March that corresponds to a drop of our default bridges usage metrics to zero.


![Default bridge OONI measurements](ooni-default-bridge-by-asn-feb.png)

Each mark in the above plot corresponds to a single OONI test, with the shape and colour of the marker determined by the AS from which the test was run. Note that while there are many reasons for why a single connection to a default bridge might fail, there is a clear drop in the reachability of bridges from all ASes in Belarus during February and March.

## BridgeDB Enumeration

We also saw an attempt to enumerate and block bridges that we distribute from BridgeDB. BridgeDB responds to user requests for bridges over three separate distribution channels:
- HTTPS by visiting https://bridges.torproject.org,
- email by sending an email to bridges@torproject.org, and
- Moat by making a domain-fronted request to https://bridges.torproject.org within Tor Browser

The bridges in our database are partitioned such that any one bridge is only ever handed out over a single distribution channel.

A volunteer performed reachability tests for a random selection of both vanilla and obfs4 bridges from each distribution channel throughout the months of February, March, and April. We found that only obfs4 bridges distributed by email were blocked. Access to bridges is limited by time and the requester's email address, meaning that the bridges a user receives is determined by hashing the user's email address with the current time interval (time intervals are defined at a granularity of 1 hour). To enumerate all of the bridges in this distribution channel, the censor would have had to repeatedly send requests for more bridges over the course of several hours, or possible from several email addresses.

The enumeration of this channel appears thorough. We tested a total of thirteen email-distributed obfs4 bridges, and every bridge that went online before February 22nd was blocked. One of the bridges added on February 22nd was blocked, while the other was not, suggesting that the enumeration occurred some time in late February and then stopped.

![Reachability of distributed obfs4 bridges](reachability-bridgedb.png)

None of the vanilla bridges we sent in any of the distribution channels were blocked during the duration of these experiments. By default, email requests to bridges@torproject.org return only obfs4 obfuscated bridges. A typical email request returns the following text:
```

[This is an automated email.]

Here are your bridges:

  obfs4 [IP] [Fingerprint] [cert]
  obfs4 [IP] [Fingerprint] [cert]

Add these bridges to your Tor Browser by opening your browser
preferences, clicking on "Tor", and then adding them to the "Provide a
bridge" field.

If these bridges are not what you need, reply to this email with one of
the following commands in the message body:

  get bridges            (Request unobfuscated Tor bridges.)
  get ipv6               (Request IPv6 bridges.)
  get transport obfs4    (Request obfs4 obfuscated bridges.)

```
this default behaviour may be the reason behind the lack of enumeration for vanilla bridges.

Since access to https://torproject.org is currently blocked in Belarus, it is likely that our HTTPS distributor available at https://bridges.torproject.org is also blocked. This leaves only Moat bridges (accessible within Tor Browser) and private bridges as the only distribution methods available to users.

## No Evidence of DPI

The reachability results and packet captures sent to us by our volunteer suggest that only endpoint IP-based blocking was used to block access to Tor bridges and relays.
The blocking fingerprint for both bridges and relays shows all TCP connections to blocked relays being terminated before the end of the TCP handshake, before any fingerprintable TLS connection could occur, suggesting that the blocking rules are IP-based. 

We also suspect that censor is not using DPI to detect and block new Tor relays due to the fact that no vanilla bridges were blocked throughout the duration of our tests.
